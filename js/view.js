function bubbleChart(s_width, s_height) {
	var width = s_width;
	var height = s_height;
	var dimw = width;
	var dimh = height;
    
	var tooltip = floatingTooltip('gates_tooltip', 240);

	var center = { x: width / 2, y: height / 2 };

	var yearCenters = {


		//--
		//--
		//--
		p01: { x: dimw/2 - 1*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p02: { x: dimw/2 - 2*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p03: { x: dimw/2 - 3*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p04: { x: dimw/2 - 4*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p05: { x: dimw/2 - 5*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p06: { x: dimw/2 - 6*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p07: { x: dimw/2 - 7*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p08: { x: dimw/2 - 8*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p09: { x: dimw/2 - 9*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		//--
		p10: { x: dimw/2 + 1*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p11: { x: dimw/2 + 2*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p12: { x: dimw/2 + 3*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p13: { x: dimw/2 + 4*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p14: { x: dimw/2 + 5*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p15: { x: dimw/2 + 6*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p16: { x: dimw/2 + 7*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p17: { x: dimw/2 + 8*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },
		p18: { x: dimw/2 + 9*(dimw/40), y: (dimh/10)*1 + (1*(dimh/10)) + 20 },


		//--
		//--
		//--
		p19: { x: dimw/2 - 1*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p20: { x: dimw/2 - 2*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p21: { x: dimw/2 - 3*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p22: { x: dimw/2 - 4*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p23: { x: dimw/2 - 5*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p24: { x: dimw/2 - 6*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p25: { x: dimw/2 - 7*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p26: { x: dimw/2 - 8*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p27: { x: dimw/2 - 9*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		//--
		p28: { x: dimw/2 + 1*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p29: { x: dimw/2 + 2*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p30: { x: dimw/2 + 3*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p31: { x: dimw/2 + 4*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p32: { x: dimw/2 + 5*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p33: { x: dimw/2 + 6*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p34: { x: dimw/2 + 7*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p35: { x: dimw/2 + 8*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },
		p36: { x: dimw/2 + 9*(dimw/38), y: (dimh/10)*1 + (2*(dimh/10)) + 20 },


		//--
		//--
		//--
		p37: { x: dimw/2 - 1*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p38: { x: dimw/2 - 2*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p39: { x: dimw/2 - 3*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p40: { x: dimw/2 - 4*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p41: { x: dimw/2 - 5*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p42: { x: dimw/2 - 6*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p43: { x: dimw/2 - 7*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p44: { x: dimw/2 - 8*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p45: { x: dimw/2 - 9*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		//--
		p46: { x: dimw/2 + 1*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p47: { x: dimw/2 + 2*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p48: { x: dimw/2 + 3*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p49: { x: dimw/2 + 4*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p50: { x: dimw/2 + 5*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p51: { x: dimw/2 + 6*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p52: { x: dimw/2 + 7*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p53: { x: dimw/2 + 8*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },
		p54: { x: dimw/2 + 9*(dimw/36), y: (dimh/10)*1 + (3*(dimh/10)) + 20 },


		//--
		//--
		//--
		p55: { x: dimw/2 - 1*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p56: { x: dimw/2 - 2*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p57: { x: dimw/2 - 3*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p58: { x: dimw/2 - 4*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p59: { x: dimw/2 - 5*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p60: { x: dimw/2 - 6*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p61: { x: dimw/2 - 7*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p62: { x: dimw/2 - 8*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p63: { x: dimw/2 - 9*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		//--
		p64: { x: dimw/2 + 1*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p65: { x: dimw/2 + 2*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p66: { x: dimw/2 + 3*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p67: { x: dimw/2 + 4*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p68: { x: dimw/2 + 5*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p69: { x: dimw/2 + 6*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p70: { x: dimw/2 + 7*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p71: { x: dimw/2 + 8*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },
		p72: { x: dimw/2 + 9*(dimw/34), y: (dimh/10)*1 + (4*(dimh/10)) + 20 },


		//--
		//--
		//--
		p73: { x: dimw/2 - 1*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p74: { x: dimw/2 - 2*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p75: { x: dimw/2 - 3*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p76: { x: dimw/2 - 4*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p77: { x: dimw/2 - 5*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p78: { x: dimw/2 - 6*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p79: { x: dimw/2 - 7*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p80: { x: dimw/2 - 8*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p81: { x: dimw/2 - 9*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		//--
		p82: { x: dimw/2 + 1*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p83: { x: dimw/2 + 2*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 },
		p84: { x: dimw/2 + 3*(dimw/32), y: (dimh/10)*1 + (5*(dimh/10)) + 20 }


	};

	var withCenters = {

        /*
		FMLN:  { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (1*(dimh/8)) + 30 },
		ARENA: { x: dimw/2 + 1*(dimw/32), y: (dimh/8)*1 + (1*(dimh/8)) + 30 },
		GANA:  { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
		PCN:   { x: dimw/2 + 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
		PDC:   { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (3*(dimh/8)) + 30 }
        */
		//--
		//--
		//--
		FMLN:  { x: (dimw/10) * 2,   y: dimh/2 + 30 },
		ARENA: { x: (dimw/10) * 3.5,   y: dimh/2 + 30 },
		GANA:  { x: (dimw/10) * 5,   y: dimh/2 + 30 },
		PCN:   { x: (dimw/10) * 6.5,   y: dimh/2 + 30 },
		PDC:   { x: (dimw/10) * 7.5,   y: dimh/2 + 30 }
	};

	var nextCenters = {


		//--
		//--
		//--
		Si: { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
		No: { x: dimw/2 + 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
	};

	var dinoCenters = {
		//--
		//--
        /*
		MASde20: { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (1*(dimh/8)) + 30 },
		MASde15: { x: dimw/2 + 1*(dimw/32), y: (dimh/8)*1 + (1*(dimh/8)) + 30 },
		MASde10: { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
		MASde05: { x: dimw/2 + 1*(dimw/32), y: (dimh/8)*1 + (2*(dimh/8)) + 30 },
		MINde05: { x: dimw/2 - 1*(dimw/32), y: (dimh/8)*1 + (3*(dimh/8)) + 30 }
        */
        
		MINde05:    { x: (dimw/10) * 2,     y: dimh/2 + 30 },
		MASde05:    { x: (dimw/10) * 3.5,   y: dimh/2 + 30 },
		MASde10:    { x: (dimw/10) * 5,     y: dimh/2 + 30 },
		MASde15:    { x: (dimw/10) * 6.5,   y: dimh/2 + 30 },
		MASde20:    { x: (dimw/10) * 7.5,   y: dimh/2 + 30 }
	};

	var yearsTitleX = {
		19941999: 160,
		19992004: width / 2,
		20042009: width - 160
	};

	//--
	var withTitleX = {
		FMLN:  (width/3) - (width/3) * (width/3),
		PCN:   (width/3) - (width/3) * (width/3),
		PDC:   (width/3) - (width/3) * (width/3),
		GANA:  (width/3) - (width/3) * (width/3),
		ARENA: (width/3) - (width/3) * (width/3)
	};

	//--
	var nextTitleX = {
		Si:  dimw/2 - 1*(dimw/32),
		No:  dimw/2 + 1*(dimw/32),
	};
	//--
	var dinoTitleX = {
		MASde20: dimw/2 - 1*(dimw/32),
		MASde15: dimw/2 - 2*(dimw/32),
		MASde10: dimw/2 - 0*(dimw/32),
		MASde05: dimw/2 + 1*(dimw/32),
		MINde05: dimw/2 + 2*(dimw/32)
	};

	var damper = 0.105;

	var svg = null;
	var bubbles = null;
	var nodes = [];

	function charge(d) {
		// return -Math.pow(d.radius, 2.0) / 8;
		return -Math.pow(d.radius, 2.0) / 8;
	}
    
	var force = d3.layout.force()
		.size([width, height])
		.charge(charge)
		.gravity(-0.01)
		.friction(0.9);


	var fillColor = d3.scale.ordinal()
		.domain(['ARENA', 'FMLN', 'GANA', 'PCN', 'PDC'])
		.range(['#3f8cc1', '#FF0000', '#ff5722', '#373fb1', '#3effcd']);

	var radiusScale = d3.scale.pow()
		// .exponent(0.5)
		// .range([2, 85]);
		.exponent(0.5)
		.range([2, 35]);

	function getRandomIntInclusive(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function createNodes(rawData) {
		// Use map() to convert raw data into node data.
		// Checkout http://learnjsdata.com/ for more on
		// working with data.
		var myNodes = rawData.map(function (d) {
			var next = "";
			var dino = "";
			var dino_text = "";

			//--
			next = d.Next;
			dino = parseInt(d.Dino);

			//--
			if(next!=null && next!=""){
				next = next.replace("Sí", "Si");
			}

			//--
			//console.log(next);

			if(dino > 20){
				dino_text = "MASde20"
			}
			else
			{
				if(dino > 15)
				{
					dino_text = "MASde15"
				}
				else
				{
					if(dino > 10){
						dino_text = "MASde10"
					}
					else
					{
						if(dino > 5)
						{
							dino_text = "MASde05"
						}
						else
						{
							dino_text = "MINde05"
						}
					}
				}
			}

			//--
			return {
				id: d.Sort,
				radius: radiusScale(radius_param),
				value: d.Many,
				name: d.Code,
				text: d.Text,
				org: d.Whom,
				group: d.Host,
				year: d.When,
				coin: d.Coin,
				host: d.Host,
				next: next,
				dino: dino_text,
                foto: d.Foto,
				// x: Math.random() * 450,
				// x: width / 2,
				x: getRandomIntInclusive( (width/2)-1200, (width/2)+1200 ),
				// x: getRandomIntInclusive( (width/2)+100, (width/2)+100 ),
				// y: Math.random() * 400,
				// y: height / 2,
				y: getRandomIntInclusive( (height/2)-1200, (height/2)+1200 ),
				// y: getRandomIntInclusive( (height/2)-100, (height/2)+100 ),
			};
		});

		// sort them to prevent occlusion of smaller nodes.
		myNodes.sort(function (a, b) { return b.value - a.value; });

		return myNodes;
	}

	var chart = function chart(selector, rawData) {
		// Use the max total_amount in the data as the max in the scale's domain
		// note we have to ensure the total_amount is a number by converting it
		// with `+`.
		var maxAmount = d3.max(rawData, function (d) { return +d.Many; });
		radiusScale.domain([0, maxAmount]);

		nodes = createNodes(rawData);
		// Set the force's nodes to our newly created nodes array.
		force.nodes(nodes);

		svg = d3.select(selector)
			.append('svg')
			.attr('width', width)
			.attr('height', height);

		bubbles = svg.selectAll('.bubble')
			.data(nodes, function (d) { return d.id; });

		var circ = bubbles.enter().append('circle')
			.classed('bubble', true)
			.attr('r', 0)
			.attr('fill', function (d) { return fillColor(d.group); })
			.attr('stroke', function (d) { return d3.rgb(fillColor(d.group)).darker(); })
			.attr('stroke-width', 1)
			//.on('click', showDetail)
			.on('mouseover', showDetail)
			.on('mouseout', hideDetail);

			circ
			.append("text")
			.attr("dy", ".3em")
			.style("text-anchor", "middle")
			.style("font-size", function(d) {
				//console.log(d);
				var text = d.name;
				//console.log('text', text);
				var lent = text.length;
				var size = d.radius/3;
				//console.log('lent', lent);
				//console.log('size', size);
				size *= 10 / lent;
				size += 1;
				//console.log(size);
				return Math.round(size)+'px';
			})
			.text(function(d) {
				// var text = d.name.substring(0, d.r / 3);
				var text = d.name;
				/*var pos = text.indexOf(' ', text.length / 2);
				if(pos < text.length / 2 + 5) {
				text = text.substring(0, pos) + "\n" + text.substring(pos+1);
				}*/
				return text;
			});

		bubbles.transition()
			// .duration(2000)
			.duration(0)
			.attr('r', function (d) { return d.radius; });

		// Set initial layout to single group.
		splitBubbles();
	};

	function groupBubbles() {
		//--
		hideYears();

		//--
        force.on('tick', function (e) {
			bubbles.each(moveToCenter(e.alpha))
				.attr('cx', function (d) { return d.x; })
				.attr('cy', function (d) { return d.y; });
		});

		//--
		force.start();
	}

	function moveToCenter(alpha) {
		return function (d) {
			d.x = d.x + (center.x - d.x) * damper * alpha;
			d.y = d.y + (center.y - d.y) * damper * alpha;
		};
	}

	function splitBubbles() {
		showYears();

		force.on('tick', function (e) {
			bubbles.each(moveToYears(e.alpha))
				.attr('cx', function (d) { return d.x; })
				.attr('cy', function (d) { return d.y; });
		});
        
        //for (var i = 0; i < 10; ++i) force.tick();
        
		force.start();
	}

	function splitWith() {
		showWith();

		force.on('tick', function (e) {
			bubbles.each(moveToWith(e.alpha))
				.attr('cx', function (d) { return d.x; })
				.attr('cy', function (d) { return d.y; });
		});

		force.start();
	}

	function splitNext() {
		showNext();

		force.on('tick', function (e) {
			bubbles.each(moveToNext(e.alpha))
				.attr('cx', function (d) { return d.x; })
				.attr('cy', function (d) { return d.y; });
		});

		force.start();
	}

	function splitDino() {
		showDino();

		force.on('tick', function (e) {
			bubbles.each(moveToDino(e.alpha))
				.attr('cx', function (d) { return d.x; })
				.attr('cy', function (d) { return d.y; });
		});

		force.start();
	}

	function moveToYears(alpha) {
		return function (d) {
			// console.log(d.year);
			// console.log(yearCenters);
			var target = yearCenters[d.year];
			if(target){
				d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
				d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
			}
			else{
				//console.log('target: ', target);
			}
		};
	}

	function moveToWith(alpha) {
		return function (d) {
			// console.log(d.year);
			// console.log(yearCenters);
			var target = withCenters[d.host];
			if(target){
				d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
				d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
			}
			else{
				//console.log('target: ', target);
			}
		};
	}

	function moveToNext(alpha) {
		return function (d) {
			// console.log(d.year);
			// console.log(yearCenters);
			var target = nextCenters[d.next];
			//--
			// console.log('d.next: '+d.next+' target-> '+target);
			// console.log(target);
			if(target){
				d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
				d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
			}
			else{
				//console.log('target: ', target);
			}
		};
	}

	function moveToDino(alpha) {
		return function (d) {
			// console.log(d.year);
			// console.log(yearCenters);
			var target = dinoCenters[d.dino];
			//--
			// console.log('d.next: '+d.next+' target-> '+target);
			// console.log(target);
			if(target){
				d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
				d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
			}
			else{
				//console.log('target: ', target);
			}
		};
	}

	function hideYears() {
		svg.selectAll('.year').remove();
	}

	function showYears() {
		// Another way to do this would be to create
		// the year texts once and then just hide them.
		var yearsData = d3.keys(yearsTitleX);
		var years = svg.selectAll('.year')
			.data(yearsData);

		years.enter().append('text')
			.attr('class', 'year')
			.attr('x', function (d) { return yearsTitleX[d]; })
			.attr('y', 40)
			.attr('text-anchor', 'middle')
			// .text(function (d) { return d; });
			.text(function (d) { return ''; });
	}

	function showWith() {
		// Another way to do this would be to create
		// the year texts once and then just hide them.
		var withData = d3.keys(withTitleX);
		var withd = svg.selectAll('.year')
			.data(withData);

		withd.enter().append('text')
			.attr('class', 'year')
			.attr('x', function (d) { return withTitleX[d]; })
			.attr('y', 40)
			.attr('text-anchor', 'middle')
			// .text(function (d) { return d; });
			.text(function (d) { return d; });
	}

	function showNext() {
		// Another way to do this would be to create
		// the year texts once and then just hide them.
		var nextData = d3.keys(withTitleX);
		var withd = svg.selectAll('.year')
			.data(nextData);

		withd.enter().append('text')
			.attr('class', 'year')
			.attr('x', function (d) { return nextTitleX[d]; })
			.attr('y', 40)
			.attr('text-anchor', 'middle')
			// .text(function (d) { return d; });
			.text(function (d) { return d; });
	}

	function showDino() {
		// Another way to do this would be to create
		// the year texts once and then just hide them.
		var dinoData = d3.keys(dinoTitleX);
		var withd = svg.selectAll('.year')
			.data(dinoData);

		withd.enter().append('text')
			.attr('class', 'year')
			.attr('x', function (d) { return dinoTitleX[d]; })
			.attr('y', 40)
			.attr('text-anchor', 'middle')
			// .text(function (d) { return d; });
			.text(function (d) { return d; });
	}



	function showDetail(d) {
		// change outline to indicate hover state.
		d3.select(this).attr('stroke', 'black');
		// console.log('d', d);
		//console.dir(d);
        var content = "<div id='foto'><img src='" + d.foto + "' /></div>" +
                        "<div id='nombre'>" + d.name + "</div>" +
                        "<div id='partido'>" + d.host + "</div>";
        /*
		var content = '<span class="name">Nombre: </span><span class="value">' +
									d.name +
									'</span><br/>' +
									'<span class="name">Patrimonio: </span><span class="value">' +
									d.coin +
									'</span><br/>' +
									'<span class="name">Partido: </span><span class="value">' +
									d.host +
									'</span><br/>' +
									'<span class="name">Reelige: </span><span class="value">' +
									d.next +
									'</span><br/>' +
									'<span class="name">Años Propietario: </span><span class="value">' +
									d.dino +
									'</span>';
        */
		tooltip.showTooltip(content, d3.event);
	}

	/*
	 * Hides tooltip
	 */
	function hideDetail(d) {
		// reset outline
		d3.select(this)
			.attr('stroke', d3.rgb(fillColor(d.group)).darker());

		tooltip.hideTooltip();
	}

	chart.toggleDisplay = function (displayName) {
		if (displayName === 'with') {
			splitWith();
		}
		else
		{
			if (displayName === 'dino') {
				splitDino();
			}
			else
			{
				if (displayName === 'next') {
					splitNext();
				}
				else
				{
					if (displayName === 'year') {
						splitBubbles();
					} else {
						groupBubbles();
					}
				}
			}
		}
	};


	// return the chart function from closure.
	return chart;
}

var w_width = window.innerWidth;
var w_height = $('#graphWrapper').height();
var radius_param = (w_width > 800)? 5 : (w_width < 500)? 2 : 3;

var myBubbleChart = bubbleChart( w_width, w_height );

function display(error, data) {
	if (error) {
		console.log(error);
	}

	myBubbleChart('#vis', data);
}

function setupButtons() {
	d3.select('#toolbar')
		.selectAll('.button')
		.on('click', function () {
			// Remove active class from all buttons
			d3.selectAll('.button').classed('active', false);
			// Find the button just clicked
			var button = d3.select(this);

			// Set it as the active button
			button.classed('active', true);

			// Get the id of the button
			var buttonId = button.attr('id');

			// Toggle the bubble chart based on
			// the currently clicked button.
			myBubbleChart.toggleDisplay(buttonId);
		});
}

function addCommas(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}

	return x1 + x2;
}
d3.csv('data/list.csv?v=ddd', display);

// setup the buttons.
setupButtons();
