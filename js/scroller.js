// using d3 for convenience, and storing a selected elements
var $container = d3.select('#scrollerWrapper');
var $graphic = d3.select('#floatingElements');
var $chart = $graphic.select('#vis');
//var $text = container.select('.scroll__text');
var $step = $container.selectAll('.screenContainer');
var $startup = true;
var $initial_border = $('#circulo-0')[0].getBoundingClientRect().left + $('#circulo-0').outerWidth(true) / 2;
var $left_border = 0;
var $right_border = 0;

// initialize the scrollama
var scroller = scrollama();

// resize function to set dimensions on load and on page resize
function handleResize() {
    //var stepHeight = Math.floor(window.innerHeight * 0.75);
    var stepHeight = window.innerHeight;
	$step.style('height', stepHeight + 'px');
    
    $('#floatingElements').css('height', stepHeight + 'px');
    //$('#mapaWrapper').css('height', stepHeight + 'px');
    $('#mapaWrapper img').css('width', (window.innerWidth * 0.75) + 'px');
}

// scrollama event handlers
function handleStepEnter(response) {
	// response = { element, direction, index }
	
    // fade in current step
	$step.classed('is-active', function (d, i) {
        return i === response.index;
	});
    $('#circulo-' + response.index).addClass('active');
    $('#circulo-' + (response.index + 1)).removeClass('active');
    
    if($startup == false && response.direction == 'up')
    {
        if(response.index == 0) $left_border = $initial_border;
        else $left_border = $('#circulo-' + (response.index - 1))[0].getBoundingClientRect().left + $('#circulo-' + (response.index - 1)).outerWidth(true) / 2;
        $right_border = $('#circulo-' + (response.index))[0].getBoundingClientRect().left + $('#circulo-' + (response.index)).outerWidth(true) / 2;

        $('#progressWrapper').animate({
            width: ( ($left_border - $initial_border) + ($right_border - $left_border) )  + 'px'
        }, 'fast');
    }
	
    // update graphic based on step here
    if($startup == false)
    {
        eval( ballTransitions[ response.index ] );
    }
    if(response.index == 7)
        $('#graphWrapper').animate({ opacity: 0 })
    else
        $('#graphWrapper').animate({ opacity: 1 })
}

function handleStepExit(response) {
	// response = { element, index, direction }
	
    if($startup == false && response.direction == 'down')
    {
        $left_border = $('#circulo-' + response.index)[0].getBoundingClientRect().left + $('#circulo-' + response.index).outerWidth(true) / 2;
        $right_border = $('#circulo-' + (response.index + 1))[0].getBoundingClientRect().left + $('#circulo-' + (response.index + 1)).outerWidth(true) / 2;

        $('#progressWrapper').animate({
            width: ( ($left_border - $initial_border) + ($right_border - $left_border) )  + 'px'
        }, 'fast');
    }

	// update graphic based on step here
	//var stepData = $step.attr('data-step')
}

function handleStepProgress(response) {
	// response = { element, index, progress }
    var elemento = d3.select(response.element);
    
    if(response.progress <= 0.6 && response.progress >= 0.4)
    {
        elemento.select('.titulo').style('opacity', 1);
        elemento.select('.textoWrap').style('opacity', 1);
    }
    else if(response.progress > 0.6)
    {
        elemento.select('.titulo').style('opacity', 1 - response.progress * 0.85);
        elemento.select('.textoWrap').style('opacity', 1 - response.progress * 0.85);
    }
    else
    {
        elemento.select('.titulo').style('opacity', response.progress * 8.5);
        elemento.select('.textoWrap').style('opacity', response.progress * 8.5);
    }
    
    if($startup == true)
    {
        elemento.select('.titulo').style('opacity', 1);
        elemento.select('.textoWrap').style('opacity', 1);
    }

}

function handleContainerEnter(response) {
	// response = { direction }
	// sticky the graphic
	$graphic.classed('is-fixed', true);
	$graphic.classed('is-bottom', false);
}

function handleContainerExit(response) {
	// response = { direction }

	// un-sticky the graphic, and pin to top/bottom of container
    $graphic.classed('is-fixed', false);
	$graphic.classed('is-bottom', response.direction === 'down');
}

// kick-off code to run once on load
function init() {
    // 1. call a resize on load to update width/height/position of elements
	handleResize();

	// 2. setup the scrollama instance
	// 3. bind scrollama event handlers (this can be chained like below)
	scroller
		.setup({
			container: '#scrollerWrapper', // our outermost scrollytelling element
			graphic: '#floatingElements', // the graphic
			text: '#scrollerWrapper', // the step container
			step: '.screenContainer', // the step elements
			offset: 0.5, // set the trigger to be 1/2 way down screen
			progress: true,
			debug: false, // display the trigger offset for testing
		})
		.onStepEnter(handleStepEnter)
		.onStepProgress(handleStepProgress)
		.onStepExit(handleStepExit)
		.onContainerEnter(handleContainerEnter)
		.onContainerExit(handleContainerExit);

	// setup resize event
	window.addEventListener('resize', handleResize);
}

// start it up
init();
